import operator
import time
import copy
import random as r
import math

class Matrix:
    """ Data structure to handle matrix operations. """

    def __init__(self, rows, cols):
        """ Sets initial values of matrix. """
        self.values = []
        self.row_ptrs =[]
        self.cols_index = []

        self.Q = None
        self.R = None
        self.rows = rows
        self.cols = cols
        self.size = (rows, cols)

    def __mul__(self, other):
        if type(other) is list:
            return self.vector_mult(other)

        if self.cols != other.rows:
            print("cols: ", self.cols, "rows: ",other.rows)
            raise NameError("Invalid Multiplication")

        result = Matrix(self.rows, other.cols)
        count = 0

        for i in range(self.rows):
            result.row_ptrs.append(count)
            for k in range(0,other.cols):
                self_col_idx = self.row_ptrs[i]
                total = 0
                while self_col_idx < self.row_ptrs[i+1]:
                    self_col = self.cols_index[self_col_idx]
                    other_col_idx = other.row_ptrs[self_col]
                    other_col = other.cols_index[other_col_idx]

                    while other_col_idx < other.row_ptrs[self_col+1] and other_col < k:
                        other_col_idx += 1
                        other_col = other.cols_index[other_col_idx]
                    if other_col == k:
                        total += self.values[self_col_idx]*other.values[other_col_idx]
                    self_col_idx += 1
                
                if total != 0:
                    result.values.append(total)
                    result.cols_index.append(k)
                    count += 1
                k += 1
        result.row_ptrs.append(count)

        return result

    def mul_second_transpose(self, other):
        """ Computes A*At when A is square"""

        if type(other) is list:
            return self.vector_mult(other)

        if self.cols != other.rows:
            print("cols: ", self.cols, "rows: ",other.rows)
            raise NameError("Invalid Multiplication")


        result = Matrix(self.rows, other.cols)
        count = 0

        for i in range(self.rows):
            result.row_ptrs.append(count)
            k = 0
            while k < other.rows:
                total = 0
                index1 = self.row_ptrs[i]
                index2 = other.row_ptrs[k]

                while index1 < self.row_ptrs[i+1] and index2 < other.row_ptrs[k+1]:
                    if self.cols_index[index1] == other.cols_index[index2]:
                        total += self.values[index1] * other.values[index2]
                        index1 += 1
                        index2 += 1
                    elif self.cols_index[index1] < other.cols_index[index2]:
                        index1 += 1
                    else:
                        index2 += 1

                if total != 0:
                    result.values.append(total)
                    result.cols_index.append(k)
                    count += 1
                k += 1
        result.row_ptrs.append(count)

        return result


    def vector_mult(self, other):
        """ Multiples the matrix by a vector 
        """

        if self.cols != len(other):
            print("Cols: ", self.cols, " Rows: ", len(other))
            raise NameError("Invalid Vector Multiplication!")

        sol = []
        for i in range(self.rows):
            k = self.row_ptrs[i]
            total = 0
            while k < self.row_ptrs[i+1]:
                total += self.values[k] * other[self.cols_index[k]]
                k += 1
            sol.append(total)

        return sol

    def vector_mult_slow(self, other):
        """ Multiples the matrix by a vector 
        TOOD: optimize away splicing, just walk the array.
        """

        if self.cols != len(other):
            print("Cols: ", self.cols, " Rows: ", len(other))
            raise NameError("Invalid Vector Multiplication!")

        sol = []
        for i in range(self.rows):
            row = self.row_splice_dict(i)
            total = 0
            for item in row:
                total += row[item]*other[item]
            sol.append(total)


        return sol

    def is_symmetric_random_vector(self, other, iterations=10):
        for i in range(iterations):
            u = r.randint(0,self.rows)
            v = r.randint(0,other.rows)
        #TODO: Finish this



    def is_symmetric(self):
        """ Checks for symmetry.  Fails fast.  
        Inefficient, does entire column splice. 
        TODO: optimizes this?
        """ 

        if self.rows != self.cols:
            return False

        for i in range(self.rows-1):
            row_values = self.row_splice_dict(i,i+1)
            col_values = self.col_splice(i,i+1)
            for item in row_values:
                if item not in col_values or row_values[item] != col_values[item]:
                    return False
        return True

    def transpose(self):
        """ Transposes a matrix. 
        Returns a new matrix, does not change the current matrix """

        result = Matrix(self.cols, self.rows)
        count = 0
        for i in range(self.cols):
            result.row_ptrs.append(count)
            cols = self.col_splice(i)
            for key in sorted(cols.keys()):
                result.cols_index.append(key)
                result.values.append(cols[key])
                count +=1
        result.row_ptrs.append(count)

        return result




    def col_splice(self, index, start=0):
        # inefficient? has to walk along col index until it finds col
        colreturn = {}
        for row in range(start, self.rows):
            splice = self.row_splice(row)
            j = self.row_ptrs[row]
            while j < self.row_ptrs[row+1] and self.cols_index[j] <= index:
                if self.cols_index[j] == index:
                    colreturn[row] = self.values[j]
                j += 1
        return colreturn

    def row_splice_dict(self, index, start=0):
        """ Returns a row from the matrix and its values """
        splice = self.cols_index[self.row_ptrs[index]:self.row_ptrs[index+1]]
        d = {}
        for i in range(self.row_ptrs[index], self.row_ptrs[index+1]):
            if self.cols_index[i] >= start:
                d[self.cols_index[i]] = self.values[i]
        return d

    def row_splice(self, index):
        """ Returns nonzero column indexs from the row """

        splice = self.cols_index[self.row_ptrs[index]:self.row_ptrs[index+1]]
        return splice

    def gmres(self, x0, e, m_max, rhs, verbose=False):
        """ Computes GMRes """

        found = False
        count = 0
        x = x0

        while not found:
            # Keep track of previous vectors
            P = []
            B = []
            R = []
            Q = []

            # Find initial residual
            r0 = list(map(operator.sub, rhs, self*x0))

            #r0 = sub_lists(rhs, self*x0)
            r0_norm = norm(r0)

            # Calculate first vector
            if r0_norm == 0:
                #Should this ever happen? Should it have been detected before this point?
                if verbose:
                    print("r0_norm = 0")
                return x

            p1 = [i/r0_norm for i in r0]
            b1 = self*p1
            P.append(p1)
            B.append(b1)

            # Minimize t
            b1tb1 = inner_product(b1)
            b1tr = inner_product(b1,r0)
            if b1tb1 == 0:
                if verbose:
                    print("b1tb1 is 0")
                return x, count
            t = b1tr/b1tb1

            # Set next iteration variables
            x = list(map(operator.add, x0, [i*t for i in p1]))
            r = list(map(operator.sub, r0, [i*t for i in b1]))
            count +=1
            m = 2
            r_norm = norm(r)

            # Calculate first vector
            a_1_norm = norm(b1)
            if a_1_norm == 0:
                print("Zero norm in Gram-Schmidt")
                return None
            q1 = [i/a_1_norm for i in b1]
            R.append([inner_product(q1,b1)])

            Q.append(q1)
            
            restart = False

            # Loop until convergence or m_max reached
            while m <= m_max and r_norm > e and not restart:
                print(m, r_norm)
                start_time = time.time()

                # Calculate next un-normalized vector
                phat_time = time.time()
                betas = [inner_product(p,r) for p in P]

                p_hat = r
                for beta in betas:
                    p_hat = list(map(operator.sub, p_hat,[i*beta for i in p_hat]))

                # Normalize new vector and add to P
                p_hat_norm = norm(p_hat)
                
                if p_hat_norm == 0:
                    if verbose:
                        print("p_hat_norm = 0")
                    restart = True
                    continue

                p_m = [i/p_hat_norm for i in p_hat]
                P.append(p_m)


                phat_end = time.time()

                if verbose:
                    print("p hat time in %.2f" %(phat_end - phat_time))

                # Compute next b_m and add to B
                b_time = time.time()
                b_m = self*p_m
                B.append(b_m)

                b_time_end = time.time()

                if verbose:
                    print("B in %.3f"% (b_time_end-b_time))

                # Minimize t in ||rm-1 - Bt|| by QR factorization

                timer = time.time()

                # calculate the rest
                ts = []
                q_hat_i = b_m
                q_hat_2 = b_m

                for q in Q:
                    t = inner_product(q, b_m)
                    #qip = inner_product(q)
                    ts.append(t)
                    q_hat_i = list(map(operator.sub, q_hat_i, [i*t for i in q]))
                    #q_hat_i = list(map(operator.sub, q_hat_i, [(i*t)/qip for i in q]))
                
                q_hat_norm = norm(q_hat_i)
                

                # q_hat norm being zero end early, even tho residual is not zero, restart fixes
                # TODO: figure out why. q is hitting less than machine precision.
                if q_hat_norm == 0:
                    #m = m_max + 1
                    restart = True
                    continue

                q_i = [i/q_hat_norm for i in q_hat_i]

                
                Q.append(q_i)
                ts.append(inner_product(q_i,b_m))
                R.append(ts)

                #Q,R,orth = gram_schmidt_fast(Q, b_m, R, r)
                timerend = time.time()
                if verbose:
                    print("QR in %.3f"% (timerend-timer))
                

                solve = time.time()
                ti = solve_QR(Q,R,r)
                solve_end = time.time()
                    

                if verbose:
                    print("Solve in %.3f "% (solve_end-solve))

                # Set next iteration variables
                list_time = time.time()

                pti = mat_mult_vect(P,ti)
                bti = mat_mult_vect(B,ti)

                mult_time = time.time()
                x = list(map(operator.add, x, pti))
                r = list(map(operator.sub, r, bti))

                list_end = time.time()
                end_time = time.time()

                if verbose:
                    print("m: %d r_norm: %.3f in %.3f seconds" %(m, r_norm, end_time-start_time))
                    print("QR Ratio: %.1f%%" %((timerend-timer)/(end_time-start_time)*100))
                    print("Solve T ratio: %.1f%%" % ( (solve_end-solve)/(end_time-start_time)*100))
                    print("Phat ratio: %.1f%%" % ((phat_end-phat_time)/(end_time-start_time)*100))
                    print("List ratio: %.1f%%"% ( (list_end-list_time)/(end_time-start_time)*100))
                    print("B ratio: %.1f%%"% ( (b_time_end-b_time)/(end_time-start_time)*100))

                r_norm = norm(r)

                count += 1
                m += 1

            # Check stopping criteria
            if norm(r) < e:
                if verbose:
                    print("hit epsilon")
                found = True
            else:
                x0 = x

        return x, count

    def gmres_slow(self, x0, e, m_max, rhs, verbose=False):
        """ Computes GMRes """

        found = False
        count = 0
        x = x0

        while not found:
            # Keep track of previous vectors
            P = []
            B = []
            R = []
            Q = []

            # Find initial residual
            r0 = sub_lists(rhs, self*x0)
            r0_norm = norm(r0)

            # Calculate first vector
            if r0_norm == 0:
                #Should this ever happen? Should it have been detected before this point?
                if verbose:
                    print("r0_norm = 0")
                return x

            p1 = [i/r0_norm for i in r0]
            b1 = self*p1
            P.append(p1)
            B.append(b1)

            # Minimize t
            b1tb1 = inner_product(b1)
            b1tr = inner_product(b1,r0)
            if b1tb1 == 0:
                if verbose:
                    print("b1tb1 is 0")
                return x, count
            t = b1tr/b1tb1

            # Set next iteration variables
            #x = add_lists(x0, int_mult_vector(t,p1))
            #r_2 = sub_lists(rhs, self*x)
            #r = sub_lists(r0, int_mult_vector(t,b1))
            x = list(map(operator.add, x0, [i*t for i in p1]))
            r = list(map(operator.sub, r0, [i*t for i in b1]))
            count +=1
            m = 2
            r_norm = norm(r)

            Q.append(b1)
            
            # Loop until convergence or m_max reached
            while m <= m_max and r_norm > e:
                start_time = time.time()

                # Calculate next un-normalized vector
                phat_time = time.time()
                betas = [inner_product(p,r) for p in P]

                p_hat = r
                for beta in betas:
                    p_hat = list(map(operator.sub, p_hat,[i*beta for i in p_hat]))

                # Normalize new vector and add to P
                p_hat_norm = norm(p_hat)
                
                if p_hat_norm == 0:
                    if verbose:
                        print("p_hat_norm = 0")
                    return x, count

                p_m = [i/p_hat_norm for i in p_hat]
                P.append(p_m)


                phat_end = time.time()

                if verbose:
                    print("p hat time in %.2f" %(phat_end - phat_time))

                # Compute next b_m and add to B
                b_time = time.time()
                b_m = self*p_m
                B.append(b_m)
                b_time_end = time.time()
                if verbose:
                    print("B in %.3f"% (b_time_end-b_time))

                # Minimize t in ||rm-1 - Bt|| by QR factorization
                timer = time.time()
                Q,R,orth = gram_schmidt_fast(Q, b_m, R, r)
                timerend = time.time()
                if verbose:
                    print("QR in %.3f"% (timerend-timer))
                
                if orth:
                    if verbose:
                        print("B already orthogonal")
                    return x, count

                solve = time.time()
                ti = solve_QR(Q,R,r)
                solve_end = time.time()


                if verbose:
                    print("Solve in %.3f "% (solve_end-solve))

                # Set next iteration variables
                list_time = time.time()
                x = list(map(operator.add, x,mat_mult_vect(P,ti)))
                r = list(map(operator.sub, r,mat_mult_vect(B,ti)))
                #x = add_lists(x, mat_mult_vect(P, ti))
                #r = sub_lists(r, mat_mult_vect(B,ti))
                list_end = time.time()
                #r = sub_lists(rhs, self*x)
                end_time = time.time()
                if verbose:
                    print("m: %d r_norm: %.3f in %.3f seconds" %(m, r_norm, end_time-start_time))
                    print("QR Ratio: %.1f%%" %((timerend-timer)/(end_time-start_time)*100))
                    print("Solve T ratio: %.1f%%" % ( (solve_end-solve)/(end_time-start_time)*100))
                    print("Phat ratio: %.1f%%" % ((phat_end-phat_time)/(end_time-start_time)*100))
                    print("List ratio: %.1f%%"% ( (list_end-list_time)/(end_time-start_time)*100))
                    print("B ratio: %.1f%%"% ( (b_time_end-b_time)/(end_time-start_time)*100))

                r_norm = norm(r)

                count += 1
                m += 1

            # Check stopping criteria
            if norm(r) < e:
                if verbose:
                    print("hit epsilon")
                found = True
            else:
                x0 = x

        return x, count
    
    def pretty_print(self):
        """ Prints matrix in array format, including zeros"""

        all_rows = "\n"
        for i in range(len(self.row_ptrs)-1):
            j = self.row_ptrs[i]
            count = 0
            row_buffer = ""
            while j < self.row_ptrs[i+1]:
                while self.cols_index[j] > count:
                   row_buffer += "0\t" 
                   count += 1
                row_buffer += str(self.values[j]) + "\t"
                count +=1
                j += 1
            while count < self.cols:
                row_buffer += "0\t" 
                count += 1
            print(row_buffer)
            all_rows += row_buffer + "\n"
            
        all_rows += "\n"
        return all_rows

    def __str__(self):
        return "\nValues:\t\t"+str(self.values)+"\nCol Indices:\t" +str(self.cols_index)+"\nRow Pointers\t" + str(self.row_ptrs)


def int_mult_vector(x, A):
    """ Multiplies and integer through an array"""

    result = [0 for j in range(len(A))] 
    for i,row in enumerate(A):
        result[i] = x * row
    return result

def add_lists(a,b):
    """ Adds two lists together """

    result = []
    for i,item in enumerate(a):
        result.append(item+b[i])
    return result

def sub_lists(a,b):
    """ Subtracts two lists """

    result = []
    for i,item in enumerate(a):
        result.append(item-b[i])
    return result

def inner_product(u, v=None):
    """ Computes the inner product of vector(s) """

    if not v:
        v = u
    if len(v) != len(u):
        raise NameError("Invalid Inner Product")
    total = 0
    for i in range(len(u)):
        total += u[i]*v[i]
    return total

def norm(u):
    """ Computes the norm of a vector """
    return  math.sqrt(inner_product(u))

def proj(a, e):
    """ Project a onto e """

    ip = inner_product(a,e)
    ipe = inner_product(e)
    
    total = ip/ipe

    return [i*total for i in e]

def backward_vector(A, b):
    """ Solves system by backwards elimination.
        Format: Must be full rank upper triangular matrix.
        If Input([[1],[2,1],[3,2,1]], [1,2,3]):

            Solves A[0] A[1] A[2]   [x]  =  [b]

                    [1]  [2]  [3]   [x3]    [1]
                         [1]  [2]   [x2]    [2]
                              [1]   [x3]    [3]

    Returns the solution vector x
    """

    #X = [0 for i in range(len(b))]
    X = {}

    for i in range(len(A)-1, -1, -1):
        total = 0
        for j in range(len(A)-1,i,-1):
            total += A[j][i]*X[j]
        div = A[i][i]
        if div == 0:
            print("0 pivot in upper triangular matrix")
            return None
        b_i = (b[i] - total)/div
        X[i] = b_i
    return X

def backward_vector_works(A, b):
    """ Solves system by backwards elimination.
        Format: Must be full rank upper triangular matrix.
        If Input([[1],[2,1],[3,2,1]], [1,2,3]):

            Solves A[0] A[1] A[2]   [x]  =  [b]

                    [1]  [2]  [3]   [x3]    [1]
                         [1]  [2]   [x2]    [2]
                              [1]   [x3]    [3]

    Returns the solution vector x
    """

    X = [0 for i in range(len(b))]

    for i in range(len(A)-1, -1, -1):
        total = 0
        for j in range(len(A)-1,i,-1):
            total += A[j][i]*X[j]
        div = A[i][i]
        if div == 0:
            print("0 pivot in upper triangular matrix")
            return None
        b_i = (b[i] - total)/div
        X[i] = b_i
    return X

def backward_vector_2(A, b):
    """ Solves system by backwards elimination.
        Format: IN REVERSE!
        Let Input([[1],[2,1],[3,2,1]], [1,2,3])

        [1]  [x1] [3,2,1]
        [2]  [x2] [2,1]
        [3]  [x3] [1]
    Returns the solution vector.
    """
    X = [0 for i in range(len(b))]
    for i in range(len(A)):
        total = 0
        for j in range(i):
            total += A[i][j]*X[-(j+1)]
        div = A[i][i]
        if div == 0:
            print("ERROR SOLVING")
            return None
        b_i = (b[-(i+1)] - total)/div
        X[-(i+1)] = b_i
    return X


def  mat_mult_vect(P, t):
    """ Multiplies a matrix by another matrix, in array format
    P is composed of column vectors, not rows!"""

    if len(P) != len(t):
        print("ERROR", len(P), len(t))
        raise NameError("matmultmat")

    x = [0 for i in range(len(P[0]))]

    for i in range(len(P)):
        for j in range(len(x)):
            x[j] += P[i][j]*t[i]

    return x

def  mat_mult_vect_works(P, t):
    """ Multiplies a matrix by another matrix, in array format
    P is composed of column vectors, not rows!"""

    x = []
    if len(P) != len(t):
        print("ERROR", len(P), len(t))
        raise NameError("matmultmat")
    for j in range(len(P[0])):
        total = 0
        for i in range(len(P)):
            total += P[i][j] * t[i]
        x.append(total)
    return x

def gram_schmidt_full(B):
    """ QR factorizes using gram schmidt and recalculates 
    Q and R from the start.

    Returns the factorized Q,R
    """

    Q = []
    R = []

    # Calculate first vector
    a_1_norm = norm(B[0])
    if a_1_norm == 0:
        print("Zero norm in Gram-Schmidt")
        return None
    q1 = [i/a_1_norm for i in B[0]]
    Q.append(q1)
    R.append([inner_product(q1,B[0])])



    # calculate the rest
    for i in range(1,len(B)):
        ts = []
        q_hat_i = B[i]

        for q in Q:
            t = inner_product(q, B[i])
            ts.append(t)
            q_hat_i = sub_lists(q_hat_i,int_mult_vector(t,q))

        q_hat_norm = norm(q_hat_i)
        if q_hat_norm == 0:
            return Q,R, True
        q_i = [i/q_hat_norm for i in q_hat_i]
        Q.append(q_i)
        ts.append(inner_product(q_i,B[i]))
        R.append(ts)

    return Q,R, False


def solve_QR(Q,R,r):
    """ Solves a Rx = Qt * r """

    qtb = []
    for q in Q:
        ip = inner_product(q,r)
        qtb.append(inner_product(q,r))

    return backward_vector(R, qtb)



def gram_schmidt_fast(Q, b_m, R, r):
    """ Adds an additional vector to the given Q and 
    calculates the next orthogonal vector """

    # Calculate first vector
    if len(Q) == 1:
        a_1_norm = norm(Q[0])
        if a_1_norm == 0:
            print("Zero norm in Gram-Schmidt")
            return None
        q1 = [i/a_1_norm for i in Q[0]]
        R.append([inner_product(q1,Q[0])])
        Q[0] = q1

    # calculate the rest
    ts = []
    q_hat_i = b_m
    for q in Q:
        t = inner_product(q, b_m)
        ts.append(t)
        q_hat_i = sub_lists(q_hat_i,int_mult_vector(t,q))
    
    q_hat_norm = norm(q_hat_i)
    
    if q_hat_norm == 0:
        return Q,R, True
    q_i = [i/q_hat_norm for i in q_hat_i]
    
    Q.append(q_i)
    ts.append(inner_product(q_i,b_m))
    R.append(ts)

    return Q,R, False



