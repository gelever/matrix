#!/usr/bin/python3
import genSym
import os
import sys
import genVect
import matrix

from parsers import entry_parser
from parsers import vector_parser


def run(argv):
    if len(argv) != 1 and len(argv) != 2:
        print("Gen takes 1 or 2 argument: matrix size, optional vector size")
        exit()
    matrix_size = int(argv[0])
    filename ="matrices/sym"+str(matrix_size)
    genSym.write_file(filename, matrix_size)

    if len(argv) > 1:
        vector_size = int(argv[1])
        vector = "matrices/"+str(vector_size)+".vect"
        genVect.write_file(vector, vector_size, 1)

if __name__ == "__main__":
    run(sys.argv[1:])
