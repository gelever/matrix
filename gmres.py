#!/usr/bin/python3

import matrix
import genVect
import random as r
import os
import sys
import time


from parsers import adj_parser
from parsers import entry_parser
from parsers import hb_parser
from parsers import vector_parser

matrix_dir = "./matrices/"

def run(argv):
    #CLI flags
    print_option = False
    file_type = None
    verbose = False
    gen = False


    #Create save directories
    if not os.path.exists(matrix_dir):
        os.makedirs(matrix_dir)

    #Handle cli flags
    if "-g" in argv:
        argv.remove("-g")
        gen = True

    if "-f" in argv:
        index = argv.index("-f") + 1
        if index < len(argv):
            file_type = argv[index]
            del argv[index]
        else:
            print("-f flag requires file format to be specified")
            exit()
        argv.remove("-f")

    if "-v" in argv:
        argv.remove("-v")
        verbose = True
    if "-p" in argv:
        argv.remove("-p")
        print_option = True


    if len(argv) != 5:
        print("GMRes Arguments:")
        print("matrix rhs x e m")
        print()
        exit()
    
    # Check file type
    if not file_type:
        file_name = argv[0]
        file_type = file_name.strip().split('.')[-1]

    # Call appropriate parser
    if file_type == "hb":
        m_info = hb_parser.read_file(matrix_dir+argv[0])
    elif file_type == "adj":
        m_info = adj_parser.read_file(matrix_dir+argv[0])
    else:
        m_info = entry_parser.read_file(matrix_dir+argv[0])
    mat = matrix.Matrix(m_info[0], m_info[1])
    mat.values = m_info[2]
    mat.cols_index = m_info[3]
    mat.row_ptrs = m_info[4]

    if gen:
        genVect.write_file(matrix_dir+argv[1], m_info[0], 1)

    b = vector_parser.read_file(matrix_dir+argv[1])

    if int(argv[2]) == 0:
        x = [0 for i in range(m_info[0])]
    else:
        x = [r.randint(0,10) for i in range(m_info[0])]

    e = float(argv[3])
    m = float(argv[4])
    
    total_time = time.time()
    print("Solving for x:")
    t_start = time.time()
    sol = mat.gmres(x,e,m,b,verbose)
    t_end = time.time()
    print("%.5f seconds" % (t_end-t_start))
    
    if print_option:
        for i,x in enumerate(sol[0]):
            print("x%i:\t%.3f"%(i+1,x))
    
    print("Solved in %d interations" % (sol[1]))



if __name__ == "__main__":
    run(sys.argv[1:])
