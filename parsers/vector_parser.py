import os

def read_file(filename):
    vector = []
    f = open(filename, 'r')
    for line in f:
        data = line.strip().split()
        vector.append(int(data[0]))
    f.close()
    return vector
