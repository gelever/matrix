import os

def read_file(filename):
    """ Produces Laplacian matrix for given graph"""

    values = []
    cols_index = []
    row_ptrs = [0]
    f = open(filename, 'r')
    row_count = 0
    col_count = 0
    item_count = 0
    diag_set = False
    diag_index = 0
    diag_total = 0

    for line in f:
        data = line.strip().split()
        if int(data[0]) != row_count: 
            #Allows zero row
            values[diag_index] = diag_total
            diag_total = 0
            diag_index = 0
            diag_set = False
            row_count += int(data[0]) - row_count
            row_ptrs.append(item_count)

        if int(data[1]) > row_count and diag_set is False:
            values.append(0)
            cols_index.append(row_count)
            diag_index = item_count
            item_count += 1
            diag_set = True

        #Assumes no trailing zero column!
        #Assumes square!
        col_count = max(col_count,row_count, int(data[1])+1)
        row_count = max(row_count,col_count, int(data[0])+1)
        if data[0] != data[1]:
            diag_total += 1
            values.append(-1.0)
            cols_index.append(int(data[1]))
            item_count += 1
        else:
            diag_total += 2
            #values.append(0.0)

    #row_count += 1

    f.close()
    row_ptrs.append(item_count)



    return row_count, col_count, values, cols_index,row_ptrs

