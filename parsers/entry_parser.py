import os

def read_file(filename, base = 0):
    base_set = False
    values = []
    cols_index = []
    row_ptrs = [0]
    f = open(filename, 'r')
    row_count = 0
    col_count = 0
    item_count = 0
    for line in f:
        data = line.strip().split()
        if not base_set:
            base = int(data[0])
            base_set = True
        if int(data[0]) != row_count: 
            #Allows zero row
            row_count += int(data[0])- base - row_count
            row_ptrs.append(item_count)
        item_count += 1

        #Assumes no zero column!
        col_count = max(col_count, int(data[1])+1- base)
        values.append(float(data[2]))
        cols_index.append(int(data[1])- base)
    row_count += 1

    f.close()
    row_ptrs.append(item_count)

    return row_count, col_count, values, cols_index,row_ptrs

