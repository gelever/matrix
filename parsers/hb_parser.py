import os

def read_file(filename,base=0):
    #TODO Implement Symmetric Format parsing, currently only unsymmetric
    #Loaded all in at once, maybe stupid
    matrix_file = open(filename,'r')
    f = matrix_file.readlines()
    matrix_file.close()

    rows = []
    row_count = int(f[2].strip().split()[1])
    col_count = int(f[2].strip().split()[2])
    row_ptr_count = int( f[1].strip().split()[1])
    col_ptr_count = int(f[1].strip().split()[2])
    nz_count = int(f[2].strip().split()[3])

    rest = f[4:]
    row_ptrs = rest[:row_ptr_count]
    cols_index = rest[row_ptr_count:(row_ptr_count+col_ptr_count)]
    values = rest[row_ptr_count+col_ptr_count:]
    values  = [lin.split() for lin in values ]
    values = [float(item) for inside in values for item in inside]
    cols_index = [lin.split() for lin in cols_index]
    row_ptrs = [lin.split() for lin in row_ptrs]
    #Collapse lists
    cols_index = [int(item)-1 for inside in cols_index for item in inside]
    row_ptrs = [int(item)-1 for inside in row_ptrs for item in inside]

    row_ptrs.append(nz_count)

    return row_count, col_count, values, cols_index, row_ptrs
