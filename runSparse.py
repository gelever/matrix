#!/usr/bin/python3

import matrix
import os
import sys
import time

from parsers import adj_parser
from parsers import entry_parser
from parsers import hb_parser

matrix_dir = "./matrices/"

def run(argv):
    #CLI flags
    print_option = False
    file_type = None


    if len(argv) < 1:
        print("Needs valid matrix!")
        exit()
    input_matrix = argv[0]

    #Create save directories
    if not os.path.exists(matrix_dir):
        os.makedirs(matrix_dir)

    #Handle cli flags
    if "-f" in argv:
        index = argv.index("-f") + 1
        if index < len(argv):
            file_type = argv[index]
            del argv[index]
        else:
            print("-f flag requires file format to be specified")
            exit()
        argv.remove("-f")

    if "-p" in argv:
        argv.remove("-p")
        print_option = True

    
    if len(argv) != 1:
        print("Takes one input matrix argument!")
        exit()

    # Check file type
    if not file_type:
        file_name = argv[0]
        file_type = file_name.strip().split('.')[-1]

    # Call appropriate parser
    if file_type == "hb":
        m_info = hb_parser.read_file(matrix_dir+argv[0])
    elif file_type == "adj":
        m_info = adj_parser.read_file(matrix_dir+argv[0])
    else:
        m_info = entry_parser.read_file(matrix_dir+argv[0])
    m_loaded = matrix.Matrix(m_info[0], m_info[1])
    m_loaded.values = m_info[2]
    m_loaded.cols_index = m_info[3]
    m_loaded.row_ptrs = m_info[4]
    
    if print_option:
        m_loaded.pretty_print()

    total_time = time.time()
    print("Transposing:")
    t_start = time.time()
    m_t = m_loaded.transpose()
    t_end = time.time()
    print(t_end - t_start, " seconds")

    print("Computing Product:")
    p_start = time.time()
    product = m_loaded*m_t
    p_end = time.time()
    print(p_end - p_start, " seconds")
    print("Calculating Symmetric")
    s_start = time.time()
    print("A*At Symmetric? :", product.is_symmetric())
    s_end = time.time()
    print(s_end - s_start, " seconds")
            
    if print_option:
        product.pretty_print()
    exit()


if __name__ == "__main__":
    run(sys.argv[1:])

